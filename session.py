# Copyright (C) 2015, see AUTHOR for contributors
#
# This file is part of CyberTimer.
#
# CyberTimer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CyberTimer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.

import gettext
_ = gettext.gettext
import logging

class Session:

	RUNNING = "running"
	PAUSED = "paused"
	IDLE = "idle"
	STARTING = "starting"
	UNAVAILABLE = "unavailable"
	PAUSING = "pausing"
	STOPPING = "stopping"

	def __init__(self, name):
		"""Create a new idle and locked session"""
		self.remainingTime = 0
		self.elapsedTime = 0
		self.state = Session.IDLE
		self.name = name
		self.locking = True
		self.locked = False
		self._lock()

	def start(self):
		logging.debug("Starting session")
		self.state = Session.RUNNING
		self._unlock()

	def pause(self):
		logging.debug("Pausing session")
		if self.state == Session.RUNNING:
			self.state = Session.PAUSED
			self._lock()

	def stop(self):
		logging.debug("Stopping session")
		if self.state == Session.RUNNING \
		or self.state == Session.PAUSED:
			self.state = Session.IDLE
			self._lock()

	def strState(self):
		if not self.locking:
			return _("Operator")
		if self.state == Session.RUNNING:
			return _("Running")
		elif self.state == Session.PAUSED:
			return _("Paused")
		elif self.state == Session.IDLE:
			return _("Locked")
		elif self.state == Session.STARTING:
			return _("Unlocked")
		elif self.state == Session.UNAVAILABLE:
			return _("Connecting to server...")


	def updateLock(self):
		if self.state == Session.RUNNING or not self.locking:
			self._unlock()
		else:
			self._lock()

	def _lock(self):
		if not self.locked:
			self.locked = True

	def _unlock(self):
		if self.locked:
			self.locked = False
