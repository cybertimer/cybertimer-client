# Copyright (C) 2015, see AUTHOR for contributors
#
# This file is part of CyberTimer.
#
# CyberTimer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CyberTimer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.

import Tkinter
import tkFont
import sys
import logging

class MainScreen:

	GUI_UNSET = 0
	GUI_LOCKED = 1
	GUI_UNLOCKED = 2


	def __init__(self, root):
		self.titleFont = tkFont.nametofont("TkHeadingFont")
		self.titleSmallSize = self.titleFont['size']
		self.titleBigSize = 36
		self.root = root
		self.root.minsize(220, 100)
		self.frame = Tkinter.Frame(self.root)
		self.sessionName = Tkinter.Label(self.frame, font=self.titleFont)
		self.sessionState = Tkinter.Label(self.frame)
		self.timeRemaining = Tkinter.Label(self.frame)
		try:
			self.logoImg = Tkinter.PhotoImage(file="logo.gif")
			self.logo = Tkinter.Label(self.frame, image=self.logoImg)
		except Tkinter.TclError:
			self.logo = None
		self.frame.pack(fill=Tkinter.BOTH, expand=True)
		self.locked = False
		self.guiState = MainScreen.GUI_UNSET
		self._updateGUI()

	def update(self, session):
		self.sessionName.config(text=session.name)
		self.sessionState.config(text=session.strState())
		time = 0
		self.timeRemaining.config(fg="#000000")
		if session.elapsedTime > 0:
			time = session.elapsedTime
			self.timeRemaining.config(fg="#007700")
		else:
			time = session.remainingTime
			if time == 0 or time > 600:
				self.timeRemaining.config(fg="#000000")
			else:
				self.timeRemaining.config(fg="#cc0000")
		minutes = (time / 60) % 60
		hours = time / 3600
		secs = time % 60
		if secs > 3:
			minutes += 1
			if minutes == 60:
				minutes = 0
				hours += 1
		self.timeRemaining.config(text="%02d:%02d"%(hours, minutes))
		if session.locked:
			self._lock()
		else:
			self._unlock()
		self._updateGUI()

	def _updateGUI(self):
		refresh = False
		refresh = (self.guiState == MainScreen.GUI_UNSET) \
			or (self.guiState == MainScreen.GUI_LOCKED and not self.locked) \
			or (self.guiState == MainScreen.GUI_UNLOCKED and self.locked)
		if refresh:
			self.sessionName.pack_forget()
			self.sessionState.pack_forget()
			self.timeRemaining.pack_forget()
			if self.logo:
				self.logo.pack_forget()
			if self.locked:
				# Fullscreen setting
				self.titleFont.configure(size=self.titleBigSize)
				if self.logo:
					self.logo.pack(fill=Tkinter.BOTH, expand=True)
				self.sessionName.pack(fill=Tkinter.BOTH, expand=True)
				self.sessionState.pack()
				self.guiState = MainScreen.GUI_LOCKED
			else:
				# Small counter setting
				self.titleFont.configure(size=self.titleSmallSize)
				self.sessionName.pack(fill=Tkinter.BOTH, expand=True)
				self.sessionState.pack(fill=Tkinter.BOTH, expand=True)
				self.timeRemaining.pack(fill=Tkinter.BOTH, expand=True)
				self.guiState = MainScreen.GUI_UNLOCKED

	def _lock(self):
		if not self.locked:
			# Go fullscreen and keep focus
			try:
				self.root.deiconify() # Restore if reduced
				self.root.lift() # Move to front
				self.root.attributes("-fullscreen", True)
				self.root.attributes("-topmost", 1) # Always on top (Windows)
				self.root.grab_set_global() # Lock focus (Linux)
				self.locked = True
				self._updateGUI()
				logging.debug("Screen locked")
			except:
				e = sys.exc_info()[0]
				logging.warning("Lock error: %s", e)
		else:
			# Just pop annoyingly
			self.root.deiconify()
			self.root.lift()

	def _unlock(self):
		if self.locked:
			# Release focus and reduce
			self.root.grab_release()
			self.root.attributes("-fullscreen", False)
			self.root.attributes("-topmost", 0)
			self.locked = False
			self._updateGUI()
			logging.debug("Screen unlocked")
