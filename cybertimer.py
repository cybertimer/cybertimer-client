# Copyright (C) 2015, see AUTHOR for contributors
#
# This file is part of CyberTimer.
#
# CyberTimer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CyberTimer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.

from mainscreen import MainScreen
from session import Session
from ctx import Ctx
import gettext
_ = gettext.gettext
gettext.bindtextdomain("cybertimer", "./i18n")
gettext.textdomain("cybertimer")
import ConfigParser

import Tkinter
import threading
import logging

updateTimer = None
session = Session("")
screen = None
cfg = {}

def startTimer():
	global updateTimer
	if updateTimer:
		updateTimer.cancel()
	updateTimer = threading.Timer(5.0, updateTmr)
	updateTimer.start()
def stopTimer():
	global updateTimer
	logging.debug("Stopping update timer")
	if updateTimer:
		updateTimer.cancel()
		updateTimer = None
		logging.debug("Update timer stopped")
def updateTmr():
	update()
	if updateTimer: # Don't restart if it was killed
		startTimer()

def update():
	ctx = Ctx(cfg['host'], cfg['port'])
	updSession = ctx.update()
	if updSession == None:
		# Network error, unavailable and such
		session.state = Session.UNAVAILABLE
		if screen:
			try:
				screen.root.after(0, screen.update, session)
			except:
				logging.warning("Could not update screen")
				logging.debug("Error on screen update", exc_info=True)
		return
	# Send back acknoledgments
	if updSession['state'] == Session.STARTING:
		# Confirm start directly
		# TODO: wait for something
		updSession = ctx.startACK()
		session.state = updSession['state']
		session.start()
	elif updSession['state'] == Session.PAUSING:
		# Send pause ACK
		updSession = ctx.pauseACK()
		session.state = updSession['state']
		session.pause()
	elif updSession['state'] == Session.STOPPING:
		# Send stop ack
		updSession = ctx.stopACK()
		session.state = updSession['state']
		session.stop()
	# Update session data
	session.name = updSession['name']
	if "remainingTime" in updSession:
		session.remainingTime = updSession['remainingTime']
		session.elapsedTime = 0
	elif "elapsedTime" in updSession:
		session.remainingTime = 0
		session.elapsedTime = updSession['elapsedTime']
	else:
		session.remainingTime = 0
		session.elapsedTime = 0
	session.state = updSession['state']
	session.locking = updSession['locking']
	session.updateLock()
	if screen:
		try:
			screen.root.after(0, screen.update, session)
		except:
			logging.warning("Could not update screen")
			logging.debug("Error on screen update", exc_info=True)

class MainWindow (Tkinter.Tk):

	def __init__(self):
		Tkinter.Tk.__init__(self)
		# Prevent closing when not in operator mode
		self.protocol("WM_DELETE_WINDOW", self.closeLock)

	def closeLock(self):
		global session
		if session.locking:
			self.iconify()
		else:
			self.destroy()

def main():
	global screen, cfg
	# Load config
	parser = ConfigParser.ConfigParser()
	parser.read("config.ini")
	cfg['host'] = parser.get("server", "host")
	cfg['port'] = parser.getint("server", "port")
	# Configure logger
	logLevel = logging.WARNING
	logFile = None
	if parser.has_option("logging", "level"):
		logCfg = parser.get("logging", "level").lower()
		if logCfg == "debug":
			logLevel = logging.DEBUG
		elif logCfg == "info":
			logLevel = logging.INFO
		elif logCfg == "warn" or logCfg == "warning":
			logLevel = logging.WARNING
		elif logCfg == "err" or logCfg == "error":
			logLevel = logging.ERROR
		elif logCfg == "critical":
			logLevel = logging.CRITICAL
	if parser.has_option("logging", "file"):
		logFile = parser.get("logging", "file")
	logFormat = "%(asctime)-15s %(levelname)s %(message)s"
	logging.basicConfig(level=logLevel, filename=logFile, format=logFormat)
	# Init network
	logging.info("Pairing with %s:%s", cfg['host'], cfg['port'])
	threading.Thread(None, update).start()
	logging.debug("Starting update timer")
	startTimer()
	# Init GUI
	tk = MainWindow()
	tk.wm_title(_("Cybertimer"))
	screen = MainScreen(tk)
	screen.update(session)
	try:
		tk.mainloop()
	except:
		logging.critical("GUI error, shutting down", exc_info=True)
	screen = None
	stopTimer()
	logging.debug("Main thread ended")
main()
