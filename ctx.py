# Copyright (C) 2015, see AUTHOR for contributors
#
# This file is part of CyberTimer.
#
# CyberTimer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CyberTimer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.

import socket
import json
import logging
import sys

class Ctx:
	"""Connectivity class to dial with the server"""

	def __init__(self, host, port):
		self.host = host
		self.port = port

	def _sendCmd(self, cmd):
		soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		soc.settimeout(3.0)
		try:
			soc.connect((self.host, self.port))
		except:
			logging.warning("Server unreachable")
			logging.debug("Socket couldn't connect", exc_info=True)
			soc.close()
			return None
		logging.debug("Connected to %s:%d", self.host, self.port)
		try:
			soc.sendall(cmd)
		except socket.error as e:
			logging.error("Unable to send %s: %s"%(cmd, e))
			soc.close()
			return None
		logging.debug("%s sent, waiting for answer", cmd)
		data = soc.recv(4096)
		logging.debug("Received %s", data)
		soc.close()
		session = None
		try:
			session = json.loads(data)
		except ValueError:
			logging.warning("Ununderstandable server answer to %s. Ignored.", cmd)
		return session

	def update(self):
		"""Request for session update from the server"""
		return self._sendCmd("session")

	def startACK(self):
		"""Send start aknowledgement to really start the session"""
		return self._sendCmd("start")
	
	def pauseACK(self):
		"""Send pause aknowledgement to really pause the session"""
		return self._sendCmd("pause")

	def stopACK(self):
		"""Send stop aknowledgement to really pause the session"""
		return self._sendCmd("stop")

